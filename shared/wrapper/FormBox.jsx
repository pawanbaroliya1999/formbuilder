import React from "react";

const FormBox = ({ children, className }) => (
  <>
    <div className="App">
      <div className="appAside" />
      <div className="appForm">
        <div className="pageSwitcher"></div>
        <div className="formTitle">{children}</div>
      </div>
    </div>
  </>
);

export default FormBox;
