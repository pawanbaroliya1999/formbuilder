module.exports = {
  // mode: 'jit',
  // purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false,
  theme: {
    extend: {
      colors: {
        wrapper: "#F6F7FB",
        primary: "#14B599",
        "primary-dark": "#12a38a",
        red: "#EB3754",
        green: "#6DB324",
        "green-dark": "#16A34A",
        yellow: "#FCB100",
        primaryLight: "#FFF8F9",
        grayLight: "#989898",
        blue: "#4AB8FF",
        yellow: "#F9CE23",
        pink: "#FD5181",
        indigo: "#312E81",
      },
    },
  },
  fontFamily: {
    body: ["Diodrum Cyrillic"],
  },
  variants: {
    display: ["responsive", "group-hover"],
    textTransform: ["group-hover"],
    transform: ["group-hover"],
    transformOrigin: ["group-hover"],
    transitionDelay: ["group-hover"],
    transitionDuration: ["group-hover"],
    transitionProperty: ["group-hover"],
    transitionTimingFunction: ["group-hover"],
    translate: ["group-hover", "hover", "focus"],
    fontSize: ["responsive", "group-hover"],
    extend: {
      transitionTimingFunction: ["hover", "focus"],
      transitionProperty: ["responsive", "motion-safe", "motion-reduce"],
    },
  },
  plugins: [],
};
