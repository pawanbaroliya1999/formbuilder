import * as yup from "yup";

export const validationField = yup.object({
  field: yup.string().required("Please enter field name."),
  type: yup.string().required("Please select field type."),
});
