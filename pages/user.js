import { withIronSession } from "next-iron-session";

import Users from "../components/user/user";

const UserRoute = () => {
  return <Users />;
};

export default UserRoute;
// export const getServerSideProps = withIronSession(async (ctx) => {});
