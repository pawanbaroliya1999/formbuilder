import withSession from "../../utils/session";

export default withSession(async (req, res) => {
  try {
    const data = req.session.get("user") || {};
    res.json({ data: data.data });
  } catch (error) {
    const { response } = error;
    res.status(response?.status || 500).json(error.data);
  }
});
