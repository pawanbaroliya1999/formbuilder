import withSession from "../../utils/session";

export default withSession(async (req, res) => {
  try {
    const data = req.body;
    const fields = { data };
    req.session.set("field", fields);
    await req.session.save();
    res.json(fields);
  } catch (error) {
    const { response } = error;
    res.status(response?.status || 500).json(error.data);
  }
});
