import withSession from "../../utils/session";

export default withSession(async (req, res) => {
  try {
    const data = req.body;
    const user = { data, isLoggedIn: true };
    req.session.set("user", user);
    await req.session.save();
    res.json(user);
  } catch (error) {
    const { response } = error;
    res.status(response?.status || 500).json(error.data);
  }
});
