import { withIronSession } from "next-iron-session";
import Admin from "../components/admin/admin";

const AdminRoute = () => {
  return <Admin />;
};

export default AdminRoute;
// export const getServerSideProps = withIronSession(async (ctx) => {});
