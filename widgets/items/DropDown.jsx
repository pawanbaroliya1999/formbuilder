import React from "react";
const DropDown = ({ label = "", data, register }) => {
  return (
    <div className="formField">
      <label className="formFieldLabel" htmlFor="name">
        {label}
      </label>

      <select className="formFieldInput" {...register}>
        <option disabled selected>
          Select Field
        </option>
        {data ? data.map((item) => <option>{item.name}</option>) : ""}
      </select>
    </div>
  );
};

export default DropDown;
