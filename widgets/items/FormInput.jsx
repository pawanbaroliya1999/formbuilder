import React from "react";

const FormInput = ({ placeholder, label, register }) => {
  return (
    <div className="formField">
      <label className="formFieldLabel" htmlFor="name">
        {label}
      </label>
      <input
        type="text"
        className="formFieldInput"
        placeholder={placeholder}
        {...register}
      />
    </div>
  );
};

export default FormInput;
