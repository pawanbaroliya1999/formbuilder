import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { validationField } from "../../validation/schema/fieldValidation";

const defaultValue = {
  field: "",
  type: "",
};

const useAdmin = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
    watch,
  } = useForm({
    resolver: yupResolver(validationField),
    defaultValues: defaultValue,
  });
  console.log("watch: ", watch("name"));

  const [fields, setFields] = useState();

  const setUserAdmin = async () => {
    const payload = {
      userType: "admin",
    };
    await fetch("api/adminUser", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(payload),
    });
  };

  useEffect(() => {
    setUserAdmin();
  }, []);

  const handleChangeUserType = async () => {
    const payload = {
      userType: "User",
    };
    await fetch("api/setTypeUser", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(payload),
    });
  };

  const onSubmit = async (data) => {
    await fetch("api/createField", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(data),
    });
  };

  return {
    fields,
    handleChangeUserType,
    register,
    handleSubmit,
    onSubmit,
    errors,
  };
};

export default useAdmin;
