import FormBox from "../../shared/wrapper/FormBox";
import DropDown from "../../widgets/items/DropDown";
import FormInput from "../../widgets/items/FormInput";
import { fieldsArray } from "../../JSOn/fields";
import useAdmin from "./useAdmin";

// this module will only allow admin to have access

const Admin = () => {
  const adminHook = useAdmin();
  return (
    <FormBox>
      <div className="formCenter">
        <form className="formFields">
          <FormInput
            label="Field name"
            id="field"
            placeholder="Enter field name"
            name="field"
            register={adminHook.register("field")}
          />
          {adminHook?.errors?.field?.message ? (
            <div className="custom-error">
              {adminHook?.errors?.field?.message}
            </div>
          ) : null}
          <DropDown
            label="select field"
            data={fieldsArray}
            register={adminHook.register("type")}
          />

          {adminHook?.errors?.type?.message ? (
            <div className="custom-error">
              {adminHook?.errors?.type?.message}
            </div>
          ) : null}
        </form>

        <div className="formField">
          <button
            className="formFieldButton"
            type="submit"
            onClick={adminHook.handleSubmit(adminHook.onSubmit)}
          >
            Add field
          </button>
          ]
        </div>

        {/* button to handle change the type of user where only admin can change its type to user
        and if any an admin without changing the user type then cannot able to access user page
         */}

        <div className="formField">
          <button
            className="formFieldButton"
            onClick={adminHook.handleChangeUserType}
          >
            Change User Type
          </button>
        </div>
      </div>
    </FormBox>
  );
};

export default Admin;
