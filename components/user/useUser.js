import { useEffect, useState } from "react";
import Router from "next/router";
import routes from "../../utils/routes";

const useUser = () => {
  const [fieldsFromAdmin, setFieldsFromAdmin] = useState({});

  const getUserType = async () => {
    await fetch("/api/getUser")
      .then((response) => response.json())
      .then((data) => {
        const { userType } = data?.data || {};
        if (userType === "admin") {
          //   redirect if user is admin
          Router.push(routes.admin);
        }
      });
  };

  const getFields = async () => {
    await fetch("/api/getFields")
      .then((response) => response.json())
      .then(({ data }) => {
        setFieldsFromAdmin(data);
      });
  };

  useEffect(() => {
    getUserType();
    getFields();
  }, []);

  return { fieldsFromAdmin };
};

export default useUser;
