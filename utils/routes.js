const routes = {
  admin: "/admin",
  user: "/user",
};

module.exports = routes;
