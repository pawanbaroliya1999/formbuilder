import { withIronSession } from "next-iron-session";

export default function withSession(handler) {
  return withIronSession(handler, {
    password: `${process.env.NEXT_SESSION_PASSWORD}`,
    cookieName: "next.js/examples/with-iron-session",
    cookieOptions: {
      secure: false,
    },
  });
}
